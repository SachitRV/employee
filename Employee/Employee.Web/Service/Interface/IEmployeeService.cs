﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Employee.Web
{
    public interface IEmployeeService
    {
        Task<ReturnModel> CreateEmployee(EmployeeViewModel mstCityViewModel);
        Task<ReturnModel> GetEmployeeList();
        Task<ReturnModel> GetEmployeeById(int EmployeeId);
        Task<ReturnModel> UpdateEmployee(EmployeeUpdateViewModel employeeUpdateViewModel);
        Task<ReturnModel> DeleteEmployee(int EmployeeId);
        Task<ReturnModel> ActivateEmployee(int EmployeeId);
        Task<ReturnModel> DeActivateEmployee(int EmployeeId);
        Task<ReturnModel> Login(LoginViewModel loginViewModel);
    }
}
