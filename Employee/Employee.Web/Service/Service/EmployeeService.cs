﻿using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Employee.Web
{
    public class EmployeeService: IEmployeeService
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IWebHostEnvironment _env;
        private ReturnModel _returnModel;
        private readonly IMapper _mapper;
        private readonly ILogger<EmployeeService> _logger;
        private readonly EmpDbContext _context;
        public EmployeeService(IEmployeeRepository employeeRepository, IHttpContextAccessor httpContextAccessor,
            IWebHostEnvironment env, IMapper mapper, ILogger<EmployeeService> logger, EmpDbContext context)
        {
            this._employeeRepository = employeeRepository;
            this._httpContextAccessor = httpContextAccessor;
            this._env = env;
            this._mapper = mapper;
            this._logger = logger;
            this._returnModel = new ReturnModel();
            this._context = context;
        }


        public async Task<ReturnModel> CreateEmployee(EmployeeViewModel employeeViewModel)
        {
            try
            {
                var entity = _mapper.Map<EmployeeDetailsEntity>(employeeViewModel);

                entity.StatusId = (Int32)Enums.RowStatus.Active;
                entity.CreatedOn = DateTime.UtcNow;
                entity.ModifiedOn = DateTime.UtcNow;
           
                _returnModel = await _employeeRepository.InsertEmployee(entity);
                if (_returnModel.IsSuccess)
                    _returnModel.Message = Constants.EMPLOYEE + Constants.CREATED_SUCCESS;
            }

            catch (Exception Ex)
            {
                _logger.LogInformation(Ex.Message);
                _returnModel.Message = Constants.SERVER_ERROR_OCCURRED;
            }

            return _returnModel;
        }

        public async Task<ReturnModel> GetEmployeeList()
        {
            try
            {
                return await _employeeRepository.GetAllEmployee();
            }

            catch (Exception Ex)
            {
                _logger.LogInformation(Ex.Message);
                _returnModel.Message = Constants.SERVER_ERROR_OCCURRED;
            }

            return _returnModel;
        }

        public async Task<ReturnModel> GetEmployeeById(int EmployeeId)
        {
            try
            {
                _returnModel = await _employeeRepository.FindEmployee(EmployeeId);
                if (!_returnModel.IsSuccess)
                    _returnModel.Message = Constants.NO_RECORD_FOUND;

            }

            catch (Exception Ex)
            {
                _logger.LogInformation(Ex.Message);
                _returnModel.Message = Constants.SERVER_ERROR_OCCURRED;
            }
            return _returnModel;
        }

        public async Task<ReturnModel> UpdateEmployee(EmployeeUpdateViewModel employeeUpdateViewModel)
        {
            try
            {
                var data = await _employeeRepository.FindEmployee(employeeUpdateViewModel.Id);
                if (data.IsSuccess)
                {
                    var record = (EmployeeDetailsEntity)data.Data;
                    record.EMPLOYEE_ID = employeeUpdateViewModel.EMPLOYEE_ID;
                    record.EMPLOYEE_LOCATION = employeeUpdateViewModel.EMPLOYEE_LOCATION;
                    record.EMPLOYEE_NAME = employeeUpdateViewModel.EMPLOYEE_NAME;
                    record.EMPLOYEE_SALARY = employeeUpdateViewModel.EMPLOYEE_SALARY;
                    record.EMPLOYEE_DESIGNATION = employeeUpdateViewModel.EMPLOYEE_DESIGNATION;
                    record.EMPLOYEE_DEPARTMENT = employeeUpdateViewModel.EMPLOYEE_DEPARTMENT;
                    record.EMPLOYEE_ID = employeeUpdateViewModel.EMPLOYEE_ID;
                    record.ID = employeeUpdateViewModel.Id;


                    
                    _returnModel = await _employeeRepository.UpdateEmployee(record);
                    if (_returnModel.IsSuccess)
                        _returnModel.Message = Constants.EMPLOYEE + Constants.UPDATED_SUCCESS;
                }

                else
                    _returnModel.Message = Constants.NO_RECORD_FOUND;
            }

            catch (Exception Ex)
            {
                _logger.LogInformation(Ex.Message);
                _returnModel.Message = Constants.SERVER_ERROR_OCCURRED;
            }
            return _returnModel;
        }

        public async Task<ReturnModel> DeleteEmployee(int EmployeeId)
        {
            try
            {
                var data = await _employeeRepository.FindEmployee(EmployeeId);
                if (data.IsSuccess)
                {
                    var record = (EmployeeDetailsEntity)data.Data;
                    record.StatusId = (Int32)Enums.RowStatus.Delete;

                    _returnModel = await _employeeRepository.DeleteEmployee(record);
                    if (_returnModel.IsSuccess)
                        _returnModel.Message = Constants.EMPLOYEE + Constants.DELTED_SUCCESS;
                }

                else
                    _returnModel.Message = Constants.NO_RECORD_FOUND;
            }

            catch (Exception Ex)
            {
                _logger.LogInformation(Ex.Message);
                _returnModel.Message = Constants.SERVER_ERROR_OCCURRED;
            }
            return _returnModel;
        }

        public async Task<ReturnModel> ActivateEmployee(int EmployeeId)
        {
            try
            {
                var data = await _employeeRepository.FindEmployee(EmployeeId);
                if (data.IsSuccess)
                {
                    var record = (EmployeeDetailsEntity)data.Data;
                    record.StatusId = (Int32)Enums.RowStatus.Active;

                    _returnModel = await _employeeRepository.ChangeStatusEmployee(record);
                    if (_returnModel.IsSuccess)
                        _returnModel.Message = Constants.EMPLOYEE + Constants.ACTIVATED_SUCCESS;
                }

                else
                    _returnModel.Message = Constants.NO_RECORD_FOUND;
            }

            catch (Exception Ex)
            {
                _logger.LogInformation(Ex.Message);
                _returnModel.Message = Constants.SERVER_ERROR_OCCURRED;
            }
            return _returnModel;
        }

        public async Task<ReturnModel> DeActivateEmployee(int EmployeeId)
        {
            try
            {
                var data = await _employeeRepository.FindEmployee(EmployeeId);
                if (data.IsSuccess)
                {
                    var record = (EmployeeDetailsEntity)data.Data;
                    record.StatusId = (Int32)Enums.RowStatus.Deactive;

                    _returnModel = await _employeeRepository.ChangeStatusEmployee(record);
                    if (_returnModel.IsSuccess)
                        _returnModel.Message = Constants.EMPLOYEE + Constants.ACTIVATED_SUCCESS;
                }

                else
                    _returnModel.Message = Constants.NO_RECORD_FOUND;
            }

            catch (Exception Ex)
            {
                _logger.LogInformation(Ex.Message);
                _returnModel.Message = Constants.SERVER_ERROR_OCCURRED;
            }
            return _returnModel;
        }

        public async Task<ReturnModel> Login(LoginViewModel loginViewModel)
        {
            try
            {
                var entity = await _context.TBL_USER.Where(x => x.USERNAME.ToLower() == loginViewModel.USERNAME.ToLower() && x.StatusId==(int)Enums.RowStatus.Active).FirstOrDefaultAsync();
                if (entity != null)
                {
                    _returnModel.IsSuccess = true;
                    _returnModel.Message = Constants.LOGIN_SUCCESS;
                }
                else
                {
                    _returnModel.IsSuccess = false;
                    _returnModel.Message = Constants.LOGIN_FAIL;
                }
            }

            catch (Exception Ex)
            {
                _returnModel.IsSuccess = false;
                _logger.LogInformation(Ex.Message);
                _returnModel.Message = Constants.SERVER_ERROR_OCCURRED;
            }

            return _returnModel;
        }
    }
}
