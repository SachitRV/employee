﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Employee.Web
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<EmployeeViewModel, EmployeeDetailsEntity>();
            CreateMap<EmployeeUpdateViewModel, EmployeeDetailsEntity>();

          
        }
    }
}
