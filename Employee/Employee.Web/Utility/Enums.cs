﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Employee.Web
{
    public class Enums
    {
        public enum RowStatus
        {
            Active = 1,
            Deactive = 2,
            Delete = 3
        }
    }
}
