﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Dapper;
using System.Threading.Tasks;

namespace Employee.Web
{
    public class BaseRepository<T> : IBaseRepository<T> where T : BaseEntity
    {
        private readonly IConfiguration _configuration;
        public BaseRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<int> Insert(string query, T Entity)
        {
            var connectionString = this.GetConnection();
            int count = 0;
            using (var con = new SqlConnection(connectionString))
            {
                try
                {
                    con.Open();
                    count = await con.ExecuteAsync(query, Entity);
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    con.Close();
                }
                return count;
            }
        }

        public async Task<int> Update(string query, T Entity)
        {
            var connectionString = this.GetConnection();
            var count = 0;

            using (var con = new SqlConnection(connectionString))
            {
                try
                {
                    con.Open();
                    count = await con.ExecuteAsync(query, Entity);
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    con.Close();
                }

                return count;
            }
        }

        public async Task<IEnumerable<T>> GetAll(string query)
        {
            var connectionString = this.GetConnection();
            IEnumerable<T> data = null;
            using (var con = new SqlConnection(connectionString))
            {
                try
                {
                    con.Open();
                    data = await con.QueryAsync<T>(query);
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    con.Close();
                }
                return data;
            }
        }

        public async Task<T> Find(string query, int Id)
        {
            var connectionString = this.GetConnection();
            T data = null;
            using (var con = new SqlConnection(connectionString))
            {
                try
                {
                    con.Open();
                    data = await con.QueryFirstOrDefaultAsync<T>(query, new { Id = Id });

                }
                catch (Exception ex)
                {

                }
                finally
                {
                    con.Close();
                }
                return data;
            }
        }

        public string GetConnection()
        {
            var connection = _configuration.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value;
            return connection;
        }
    }
    }
