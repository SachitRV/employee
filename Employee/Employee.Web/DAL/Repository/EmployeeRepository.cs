﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Employee.Web
{
    public class EmployeeRepository : BaseRepository<EmployeeDetailsEntity>, IEmployeeRepository
    {
        private ReturnModel _returnModel;
        public EmployeeRepository(IConfiguration _configuration) : base(_configuration)
        {
            _returnModel = new ReturnModel();
            _returnModel.Message = Constants.SERVER_ERROR_OCCURRED;
        }
        public async Task<ReturnModel> InsertEmployee(EmployeeDetailsEntity Entity)
        {
            int result = 0;
            string query = @"Insert into TBL_EMPLOYEE_DETAILS (EMPLOYEE_ID,EMPLOYEE_NAME,EMPLOYEE_DESIGNATION,EMPLOYEE_SALARY,EMPLOYEE_DEPARTMENT,EMPLOYEE_LOCATION,EMPLOYEE_DOB, StatusId, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy) values(" +
                             "@EMPLOYEE_ID,@EMPLOYEE_NAME,@EMPLOYEE_DESIGNATION,@EMPLOYEE_SALARY,@EMPLOYEE_DEPARTMENT,@EMPLOYEE_LOCATION,@EMPLOYEE_DOB, @StatusId, @CreatedOn, @CreatedBy, @ModifiedOn, @ModifiedBy);" +
                             "SELECT CAST(SCOPE_IDENTITY() as int);";

            result = await Insert(query, Entity);
            if (result > 0)
            {
                _returnModel.IsSuccess = true;
                _returnModel.Data = result;
            }

            return _returnModel;
        }

        public async Task<ReturnModel> UpdateEmployee(EmployeeDetailsEntity Entity)
        {
            int result = 0;
            string query = "Update TBL_EMPLOYEE_DETAILS SET EMPLOYEE_ID=@EMPLOYEE_ID,EMPLOYEE_NAME=@EMPLOYEE_NAME,EMPLOYEE_DESIGNATION=@EMPLOYEE_DESIGNATION,EMPLOYEE_SALARY=@EMPLOYEE_SALARY,EMPLOYEE_DEPARTMENT=@EMPLOYEE_DEPARTMENT,EMPLOYEE_LOCATION=@EMPLOYEE_LOCATION,EMPLOYEE_DOB=@EMPLOYEE_DOB, ModifiedOn = @ModifiedOn, ModifiedBy = @ModifiedBy where " +
                                 "ID = @ID";

            result = await Update(query, Entity);
            if (result > 0)
            {
                _returnModel.IsSuccess = true;
                _returnModel.Data = result;
            }

            return _returnModel;
        }

        public async Task<ReturnModel> DeleteEmployee(EmployeeDetailsEntity Entity)
        {
            int result = 0;
            string query = "Update TBL_EMPLOYEE_DETAILS SET StatusId = @StatusId, ModifiedOn = @ModifiedOn, ModifiedBy = @ModifiedBy where " +
                                "ID = @ID";

            result = await Update(query, Entity);
            if (result > 0)
            {
                _returnModel.IsSuccess = true;
                _returnModel.Data = result;
            }

            return _returnModel;
        }

        public async Task<ReturnModel> ChangeStatusEmployee(EmployeeDetailsEntity Entity)
        {
            int result = 0;
            string query = "Update TBL_EMPLOYEE_DETAILS SET StatusId = @StatusId, ModifiedOn = @ModifiedOn, ModifiedBy = @ModifiedBy where " +
                                "ID = @ID";

            result = await Update(query, Entity);
            if (result > 0)
            {
                _returnModel.IsSuccess = true;
                _returnModel.Data = result;
            }

            return _returnModel;
        }

        public async Task<ReturnModel> GetAllEmployee()
        {
            string query = "Select * FROM  TBL_EMPLOYEE_DETAILS WHERE StatusId IN (1,2)";

            var data = await GetAll(query);
            if (data != null)
            {
                _returnModel.IsSuccess = true;
                _returnModel.Data = data;
            }

            return _returnModel;
        }

        public async Task<ReturnModel> FindEmployee(int Id)
        {
            string query = "Select * FROM  TBL_EMPLOYEE_DETAILS where ID = @ID";

            var data = await Find(query, Id);
            if (data != null)
            {
                _returnModel.IsSuccess = true;
                _returnModel.Data = data;
            }

            return _returnModel;
        }
    }
}
