﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Employee.Web
{
    public interface IEmployeeRepository
    {
        Task<ReturnModel> InsertEmployee(EmployeeDetailsEntity Entity);
        Task<ReturnModel> UpdateEmployee(EmployeeDetailsEntity Entity);
        Task<ReturnModel> DeleteEmployee(EmployeeDetailsEntity Entity);
        Task<ReturnModel> ChangeStatusEmployee(EmployeeDetailsEntity Entity);
        Task<ReturnModel> GetAllEmployee();
        Task<ReturnModel> FindEmployee(int Id);
    }
}
