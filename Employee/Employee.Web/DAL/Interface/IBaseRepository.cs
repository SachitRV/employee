﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Employee.Web
{
    public interface IBaseRepository<T> where T : BaseEntity
    {
        Task<int> Insert(string query, T Entity);

        Task<int> Update(string query, T Entity);

        Task<IEnumerable<T>> GetAll(string query);

        Task<T> Find(string query, int Id);

    }
}
