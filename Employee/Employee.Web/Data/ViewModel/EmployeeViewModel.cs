﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Employee.Web
{
    public class EmployeeViewModel
    {
        [Required]
        [StringLength(50)]
        [RegularExpression(@"^[a-zA-Z]{1}[0-9]{3}", ErrorMessage = "InValid Employee Id ")]
        public string EMPLOYEE_ID { get; set; }
        [Required]
        [StringLength(50)]
        [RegularExpression(@"^[A-Za-z]+$",ErrorMessage ="InValid Employee Name")]
        public string EMPLOYEE_NAME { get; set; }
        [Required]
        [StringLength(50)]
        [RegularExpression(@"^([^0-9]*)$",ErrorMessage ="InValid  Designation")]
        public string EMPLOYEE_DESIGNATION { get; set; }
        [Required]
        public decimal EMPLOYEE_SALARY { get; set; }
        [Required]
        [StringLength(50)]
        //[RegularExpression(@"^[a-zA-Z0-9]{4,10}$",ErrorMessage ="InValid  Department")]
        public string EMPLOYEE_DEPARTMENT { get; set; }
        [Required]
        [StringLength(50)]


        public string EMPLOYEE_LOCATION { get; set; }
        [DataType(DataType.Date)]
        public DateTime EMPLOYEE_DOB { get; set; }
    }
    public class EmployeeUpdateViewModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        [RegularExpression(@"^[a-zA-Z]{1}[0-9]{3}", ErrorMessage = "InValid Employee Id ")]
        public string EMPLOYEE_ID { get; set; }
        [Required]
        [StringLength(50)]
        [RegularExpression(@"^[A-Za-z]+$", ErrorMessage = "InValid Employee Name")]
        public string EMPLOYEE_NAME { get; set; }
        [Required]
        [StringLength(50)]
        [RegularExpression(@"^([^0-9]*)$", ErrorMessage = "InValid  Designation")]
        public string EMPLOYEE_DESIGNATION { get; set; }
        [Required]
        public decimal EMPLOYEE_SALARY { get; set; }
        [Required]
        [StringLength(50)]
        //[RegularExpression(@"^[a-zA-Z0-9]{4,10}$", ErrorMessage = "InValid  Department")]
        public string EMPLOYEE_DEPARTMENT { get; set; }
        [Required]
        [StringLength(50)]


        public string EMPLOYEE_LOCATION { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime EMPLOYEE_DOB { get; set; }

    }
}
