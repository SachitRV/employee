﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Employee.Web
{
    public class LoginViewModel
    {
        [Required]
        public string USERNAME { get; set; }

        [DataType(DataType.Password)]
        [Required]
        public string USERPASSWORD { get; set; }
    }
}
