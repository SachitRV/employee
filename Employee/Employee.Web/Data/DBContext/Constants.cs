﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Employee.Web
{
    public class Constants
    {
        public static readonly string SERVER_ERROR_OCCURRED = "Server Error!! Please try After sometime";
        public static readonly string CREATED_SUCCESS = " created successfully";
        public static readonly string UPDATED_SUCCESS = " updated successfully";
        public static readonly string DELTED_SUCCESS = " deleted successfully";
        public static readonly string ACTIVATED_SUCCESS = " activated successfully";
        public static readonly string DEACTIVATED_SUCCESS = " de-activated successfully";
        public static readonly string NO_RECORD_FOUND = "No record found";
        public static readonly string EMPLOYEE = "Employee";
        public static readonly string LOGIN_SUCCESS = "Logged In SuccessFully";
        public static readonly string LOGIN_FAIL = "Failed To Log In";
    }
}
