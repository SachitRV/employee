﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Employee.Web.Enums;

namespace Employee.Web
{
    public class BaseEntity
    {
        public int StatusId { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }
}
