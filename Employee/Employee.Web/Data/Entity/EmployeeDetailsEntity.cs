﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Employee.Web
{
    public class EmployeeDetailsEntity:BaseEntity
    {
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity), Key()]
        public int ID { get; set; }
        public string EMPLOYEE_ID { get; set; }
        public string EMPLOYEE_NAME { get; set; }
        public string EMPLOYEE_DESIGNATION { get; set; }
        public decimal EMPLOYEE_SALARY { get; set; }
        public string EMPLOYEE_DEPARTMENT { get; set; }
        public string EMPLOYEE_LOCATION { get; set; }
        public DateTime EMPLOYEE_DOB { get; set; }
    }
}
