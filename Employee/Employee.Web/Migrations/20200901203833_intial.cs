﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Employee.Web.Migrations
{
    public partial class intial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TBL_EMPLOYEE_DETAILS",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EMPLOYEE_ID = table.Column<string>(nullable: true),
                    EMPLOYEE_NAME = table.Column<string>(nullable: true),
                    EMPLOYEE_DESIGNATION = table.Column<string>(nullable: true),
                    EMPLOYEE_SALARY = table.Column<decimal>(nullable: false),
                    EMPLOYEE_DEPARTMENT = table.Column<string>(nullable: true),
                    EMPLOYEE_LOCATION = table.Column<string>(nullable: true),
                    EMPLOYEE_DOB = table.Column<DateTime>(nullable: false),
                    StatusId = table.Column<int>(nullable: false),

                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                 
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TBL_EMPLOYEE_DETAILS", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "TBL_USER",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    USERNAME = table.Column<string>(nullable: true),
                    USERPASSWORD = table.Column<string>(nullable: true),
                    StatusId = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
              
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TBL_USER", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TBL_EMPLOYEE_DETAILS");

            migrationBuilder.DropTable(
                name: "TBL_USER");
        }
    }
}
