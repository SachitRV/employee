﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Employee.Web.Controllers
{
    [Area("User")]
    public class EmployeeController : Controller
    {
        private readonly IEmployeeService _employeeService;
        private ReturnModel _returnModel;
        public EmployeeController(IEmployeeService employeeService)
        
        {
            this._employeeService = employeeService;
            this._returnModel = new ReturnModel();
            _returnModel.Message = Constants.SERVER_ERROR_OCCURRED;
        }
        public async Task<ActionResult> Index()
        {
          

            List<EmployeeDetailsEntity> model = new List<EmployeeDetailsEntity>();
            _returnModel = await _employeeService.GetEmployeeList();
      
   
            if (_returnModel.IsSuccess)
            {
                ViewBag.EmployeeList = _returnModel.Data;
                return View("~/Areas/User/Views/EmpForm/Index.cshtml");
            }

            else
            {
                ViewBag.EmpList = model;
                TempData["Msg"] = _returnModel.Message;
                return View("~/Areas/User/Views/EmpForm/Index.cshtml");
            }
        }

        [HttpGet]
        public ActionResult AddEmployee()
        {
            return View("~/Areas/User/Views/EmpForm/Index.cshtml", new EmployeeDetailsEntity());
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> AddEmployee(EmployeeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                _returnModel.Message = ModelState.FirstOrDefault(x => x.Value.Errors.Any()).Value.Errors.FirstOrDefault().ErrorMessage;
                return Json(_returnModel);
            }

            _returnModel = await _employeeService.CreateEmployee(model);
            return Json(_returnModel);
        }


        [HttpGet]
        public async Task<ActionResult> EditEmployee(int EmployeeId)
        {
            ParentModel parent = new ParentModel();
            EmployeeDetailsEntity model = new EmployeeDetailsEntity();
            _returnModel = await _employeeService.GetEmployeeById(EmployeeId);

            List<EmployeeDetailsEntity> res = new List<EmployeeDetailsEntity>();
            ViewBag.EmployeeList = res;

            if (_returnModel.IsSuccess)
            {            
                return View("~/Areas/User/Views/EmpForm/Index.cshtml", _returnModel.Data);
            }
            else
            {
                TempData["Msg"] = _returnModel.Message;
                return View("~/Areas/User/Views/EmpForm/Index.cshtml", model);
            }
        }


        [HttpPost]
        public async Task<JsonResult> EditEmployee(EmployeeUpdateViewModel model)
        {

            if (!ModelState.IsValid)
            {
                _returnModel.Message = ModelState.FirstOrDefault(x => x.Value.Errors.Any()).Value.Errors.FirstOrDefault().ErrorMessage;
                return Json(_returnModel);
            }

            _returnModel = await _employeeService.UpdateEmployee(model);
            return Json(_returnModel);
        }


        public async Task<JsonResult> DeleteEmployee(int EmployeeId)
        {
            _returnModel = await _employeeService.DeleteEmployee(EmployeeId);
            return Json(_returnModel);
        }


        public async Task<JsonResult> ActivateEmployee(int EmployeeId)
        {
            _returnModel = await _employeeService.ActivateEmployee(EmployeeId);
            return Json(_returnModel);
        }


        public async Task<JsonResult> DeActivateEmployee(int EmployeeId)
        {
            _returnModel = await _employeeService.DeActivateEmployee(EmployeeId);
            return Json(_returnModel);
        }
    }
}
