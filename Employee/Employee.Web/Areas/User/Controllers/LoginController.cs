﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Employee.Web.Controllers
{
    [Area("User")]
    public class LoginController : Controller
    {
        private readonly IEmployeeService _employeeService;
        private ReturnModel _returnModel;
        public LoginController(IEmployeeService employeeService)
        {
            this._employeeService = employeeService;
            this._returnModel = new ReturnModel();
            _returnModel.Message = Constants.SERVER_ERROR_OCCURRED;
        }
        public async Task<ActionResult> Index()
       {         
                return View("~/Areas/User/Views/Login/Index.cshtml");        
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> LoggedIn(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                _returnModel.Message = ModelState.FirstOrDefault(x => x.Value.Errors.Any()).Value.Errors.FirstOrDefault().ErrorMessage;
                return Json(_returnModel);
            }
            _returnModel = await _employeeService.Login(model);
            if(_returnModel.IsSuccess)
            {
                //return View("~/Areas/User/Employee/Index.cshtml");
                return Json(_returnModel);
            }
            return Json(_returnModel);
        }
    }
}
