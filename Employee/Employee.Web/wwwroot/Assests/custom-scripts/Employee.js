﻿
$(document).ready(function () {
    $('#tblEmployee').DataTable();
});


function InsertUpdateEmployee() {
    debugger;
    var hasError = Validate("#divEmployeeContainer");
    if (!hasError) {

        var Id = $('#ID').val();
        var url = '/User/Employee/AddEmployee';
        if (Id > 0) {
            url = '/User/Employee/EditEmployee';
        }

        $.post(url, $('#frmEmployee').serialize(), function (data) {
            if (data.isSuccess == true) {
                swal({
                    title: "Success",
                    text: data.message,
                    icon: "success",
                })
                    .then((refresh) => {
                        window.location.href = '/User/Employee/Index';
                    });
            }
            else {
                swal("Oops", data.message, "error");
            }
        });
    }
};


function ActivateEmployee(EmployeeId) {
    $.post('/User/Employee/ActivateEmployee', { EmployeeId: EmployeeId }, function (data) {
        if (data.isSuccess == true) {
            swal({
                title: "Success",
                text: data.message,
                icon: "success",
            })
                .then((refresh) => {
                    window.location.href = '/User/Employee/Index';
                });
        }
        else {
            swal("Oops", data.message, "error");
        }
    });
};


function DeactivateEmployee(EmployeeId) {
    $.post('/User/Employee/DeactivateEmployee', { EmployeeId: EmployeeId }, function (data) {
        if (data.isSuccess == true) {
            swal({
                title: "Success",
                text: data.message,
                icon: "success",
            })
                .then((refresh) => {
                    window.location.href = '/User/Employee/Index';
                });
        }
        else {
            swal("Oops", data.message, "error");
        }
    });
};

function DeleteEmployee(EmployeeId) {
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this file!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                $.post('/User/Employee/DeleteEmployee', { EmployeeId: EmployeeId }, function (data) {
                    if (data.isSuccess == true) {
                        swal({
                            title: "Success",
                            text: data.message,
                            icon: "success",
                        })
                            .then((refresh) => {
                                window.location.href = '/User/Employee/Index';
                            });
                    }
                    else {
                        swal("Oops", data.message, "error");
                    }
                });
            }
            else {
                swal("Your file is safe!");
            }
        });
};