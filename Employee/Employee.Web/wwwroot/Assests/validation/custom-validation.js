﻿function Validate(divContainerID) {   
    var labelText = "";
    var errInput = $(divContainerID).find(".errBox");
    errInput.removeClass("errBox");
    var errLabel = $(divContainerID).find("span.error");
    $(errLabel).remove();
    var hasError = false;

    //===========Required Field Validation==============
    var validate = divContainerID + " .requiredField";
    $(validate).each(function () {
        if (jQuery.trim($(this).val()) == '') {
            labelText = $(this).attr('title');
            $(this).parent().append('<span class="error errMessage">' + labelText + ' is Mandatory.</span>');
            $(this).addClass("errBox");           
            hasError = true;
        }
    });  
 

    //===========Validate Only Decimal Values===================
    var validateDecimalValue = divContainerID + " .decimalValue";
    $(validateDecimalValue).each(function () {
        if ($(this).val() != '') {
            var decimalValueReg = /^\d+(\.\d{1,2})?$/;
            if (!decimalValueReg.test(jQuery.trim($(this).val()))) {
                labelText = $(this).attr('title');
                $(this).parent().append('<span class="error errorMessage">Invalid ' + labelText + '.(Only numbers are allowed.)</span>');
                hasError = true;
            }
        }
    });    

    return hasError;
}